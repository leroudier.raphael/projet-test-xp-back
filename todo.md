## Todo
    - faire des schema et leur validateur
    - faire set password (avec le salt)
    - faire la fonction de set token
    - faire fonction de get token
    - faire fonction log
    - faire les services
    - mettre en place les routes
    - faire les controllers

## Doing
    - test model et DAO

## Done
    - creation des dao
    - mise en place des tests
    - model & interface user
    - model & interface event
    - faire la connection a mongo db
    - connection avec le server

## Bugs
    - ligne d'erreur avec le debug "sourceMap" (voir avec Jean)