import { Router } from 'express';
import { EventController } from '../controller/eventController';
import { authenticated } from '../middleWare/userMW';
const eventsRouter = Router();

eventsRouter.get('/findall', EventController.findIncoming)
eventsRouter.post('/save', authenticated, EventController.save)

export default eventsRouter;
