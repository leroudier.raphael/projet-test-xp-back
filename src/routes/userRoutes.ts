import { Router } from 'express';
import { UserController } from '../controller/userController'
const usersRouter = Router();

usersRouter.post('/save', UserController.save)
// usersRouter.post('/findid', UserController.findId)

export default usersRouter;