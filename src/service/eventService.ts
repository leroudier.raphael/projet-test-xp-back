
import { Event } from "../models/event";
import { EventDao } from "../repository/eventDao";

export class EventService {
    static save(input: object): Promise<any> {
        if (this.checkInput(input)) {

            let event = new Event(input["name"], input["startEvent"], input["endEvent"], input["location"], input["details"])

            return new Promise(async function (resolve, reject) {

                try {
                    let response = await EventDao.save(event)
                    resolve(response)
                } catch (err) {
                    reject(err)
                }
            })
        }
    }

    static findIncoming(): Promise<Event[]> {
        return new Promise(async function (resolve, reject) {
            try {
                let response = await EventDao.findIncoming()
                console.log("here 1 response ->", response)

                let eventArray= []

                for(let i=0; i < response.length; i++){
                    eventArray[i]= await response[i].map((x: Event) => { return new Event(x.name, x.startEvent, x.endEvent, x.location, x.details, x._id) })
                }
                console.log("here 2 event array->", eventArray)
                
                resolve(eventArray)
            } catch (err) {
                reject(err)
            }
        })
    }


    static checkInput(input: object):boolean {
        if (typeof input["name"] != 'string') {
            throw new Error("bad name in input")
        }
        if (Object.prototype.toString.call(input["startEvent"]) != '[object Date]') {
            throw new Error("bad start date in input")
        }
        if (Object.prototype.toString.call(input["endEvent"]) != '[object Date]') {
            throw new Error("bad end date in input")
        }
        if (typeof input["location"] != 'string') {
            throw new Error("bad location in input")
        }
        if (typeof input["details"] != 'string' && input["details"] != null) {
            throw new Error("bad details in input")
        }
        return true
    }
}
