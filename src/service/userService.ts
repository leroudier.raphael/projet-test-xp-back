import { User } from "../models/user";
import { UserDao } from "../repository/userDao";
import { userSchema } from "../schema/userSchema";
import AJV from "ajv";

const ajv = new AJV({ allErrors: true })

export class UserService {
    static save(input: User): Promise<any> {
        return new Promise(async function (resolve, reject) {
            try {
                if (!ajv.validate(userSchema, input)) throw new Error(JSON.stringify(ajv.errors));
                const user = new User(null, null, null, null, input);
                if (input.password && input['password2'] && input.password === input['password2']) {
                    await user.setPassword(input.password, true)
                    delete user.password2
                }
                for (const key in user) {
                    if (user[key] == null) delete user[key]
                }
                // not null proerty
                await UserDao.save(user)
                resolve(user)
            } catch (err) {
                reject(err)
            }
        })
    }

    static findByMail(mail): Promise<User> {
        return new Promise(async function (resolve, reject) {
            try {
                let response = await UserDao.find({ 'mail': mail })
                resolve(response)
            } catch (err) {
                reject(err)
            }
        })
    }
}
