import bcrypt from 'bcrypt';
import { UserDao } from '../repository/userDao';
import { jwtService } from '../service/jwtService';
import { ObjectId } from 'mongodb';
import { User } from '../models/user';

const EMAIL_REGEX = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
const PASS_REGEX = new RegExp(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/)

export class LoginService {
    static login(mail = null, password = null, token = null) {
        return new Promise(async (resolve, reject) => {
            try {
                let user: User;

                if (!token) {
                    if (!EMAIL_REGEX.test(mail)) throw new Error('Mail Invalid')
                    if (!PASS_REGEX.test(password)) throw new Error('Password Invalid')
                    user = await UserDao.find({ 'mail': mail })
                    if (!user) throw new Error('user Invalid')
                    let bcryptRep = await bcrypt.compare(password, user.password)
                    if (!bcryptRep) throw new Error('Password ou mail erroné')

                } else {
                    if (!token) throw new Error('Token Invalid')
                    let resultToken = await jwtService.verifToken(token)
                    if (!resultToken) throw new Error('jwtService error')
                    user = await UserDao.find({ _id: new ObjectId(resultToken['_id']) })
                }
                resolve({
                    user: user.light(),
                    token: jwtService.generateToken(user),
                })
            } catch (err) {
                reject(err)
            }
        })
    }
}