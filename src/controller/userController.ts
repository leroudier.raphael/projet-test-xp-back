import { User } from "../models/user";
import { UserService } from "../service/userService";

export class UserController {
    static async save(req, res, next) {
        UserService.save(req.body).then(
            (x: User) => {
                return res.status(201).json(x);
            }
        ).catch(
            (error) => {
                return res.status(400).json({
                    error: error
                });
            }
        );
    }
}


