import { LoginService } from '../service/loginService';

export async function login(req, res) {
    LoginService.login(req.body.mail, req.body.password, req.body.token).then(
        (x: any) => {
            return res.status(201).json(x);
        }
    ).catch(
        (error) => {
            return res.status(400).json(error);
        }
    );
}