import { Event } from "../models/event";
import { EventService } from "../service/eventService";

export class EventController {
    static async save(req, res, next) {
        EventService.save(req.body.event).then(
            (x: Event) => {
                return res.status(201).json(x);
            }
        ).catch(
            (error) => {
                return res.status(400).json({
                    error: error
                });
            }
        );
    }
    static async findIncoming(req, res, next) {
        EventService.findIncoming().then(
            (x: Event[]) => {
                return res.status(201).json(x);
            }
        ).catch(
            (error) => {
                return res.status(400).json({
                    error: error
                });
            }
        );
    }
}


