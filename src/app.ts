import express from 'express';
import helmet from 'helmet';
import { getConnection } from '../configMongo';
import router from './routes/router';
import cors from 'cors'
import bodyParser from 'body-parser';
import { login } from './controller/loginController';
const app = express();
const port = 3000;
app.use(helmet());
app.use(cors());
app.use(bodyParser
    .urlencoded({ extended: true }));
app.use(bodyParser
    .json({ limit: '50mb' }));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'content-type, Access-Control-Allow-Headers, Authorization, X-Requested-With, apitoken, usertoken, idfirm');
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    next();
});
app.use('/', router)

export default app