import { ObjectId } from "mongodb";
import { UserDao } from "../repository/userDao";
import { jwtService } from "../service/jwtService";

export async function authenticated(req, res, next) {
    try {
        const token = req.headers.token;
        if (!token) throw new Error('No token User')
        let reponse = await jwtService.verifToken(token)
        if (!reponse) throw new Error('Token Invalid')
        let user = await UserDao.find({ "_id": new ObjectId(reponse['_id']) })
        if (!user) throw new Error('User Invalid')
        next()
    } catch (err) {
        return res.status(403).json(err)
    }
}