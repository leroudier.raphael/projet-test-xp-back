import { getConnection } from '../../configMongo';
import { Event } from '../models/event';


export class EventDao {

    static findIncoming(): Promise<any> {
        return new Promise(async function (resolve, reject) {
            try {
                const db = await getConnection()
                const response = await db.collection('events').find({'endEvent' : {$gt: new Date() }}).toArray();
                if (!response) throw new Error("requête non trouvée");
                resolve(response)
            } catch (err) {
                reject(err)
            }
        })
    }

    static save(event: Event): Promise<Event> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                const response: any = await db.collection('events').replaceOne({ _id: event._id }, event, { upsert: true })
                if (!response) throw new Error("requête non trouvée");
                resolve(event)
            } catch (err) {
                reject(err)
            }
        })
    }
}