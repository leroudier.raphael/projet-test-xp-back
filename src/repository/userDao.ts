import { ObjectId } from 'mongodb';
import { getConnection } from '../../configMongo';
import { User } from '../models/user';

const collection: string = 'users'

export class UserDao {
    static find(request: object): Promise<any> {
        return new Promise(async function (resolve, reject) {
            try {
                const db = await getConnection();
                const response = await db.collection(collection).findOne(request)
                if (!response) throw new Error("requête non trouvée")
                resolve(new User(null, null, null, null, response))
            } catch (err) {
                reject(err)
            }
        })
    }

    static save(user: User): Promise<User> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                const response: any = await db.collection(collection).replaceOne({ _id: user._id }, user, { upsert: true })
                if (!response) throw new Error("requête non trouvée");
                resolve(user)
            } catch (err) {
                reject(err)
            }
        })
    }
}