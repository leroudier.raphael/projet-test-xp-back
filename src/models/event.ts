import { ObjectId } from "mongodb";

export interface Event {
    _id?: ObjectId;
    name: string;
    startEvent: Date;
    endEvent: Date;
    location: string;
    details?: string;
}

export class Event {
    constructor(name: string, startEvent: Date, endEvent: Date, location: string, details: string = null, _id?: ObjectId) {
        if (_id && ObjectId.isValid(_id)) {
            this._id = typeof _id == 'string' ? new ObjectId(_id) : _id

        } else {
            this._id = new ObjectId()
        }

        this.name = name;
        this.startEvent = startEvent;
        this.endEvent = endEvent;
        this.location = location;
        this.details = details;
    }
}
