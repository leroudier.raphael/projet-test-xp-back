import { Db, MongoClient } from "mongodb";

//const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'project_test';

//Options
var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 20
};

export function getConnection(): Promise<Db> {
    return new Promise(async function (resolve, reject) {
        MongoClient.connect(url, options, function (err, client) {
            const db = client.db(dbName);
            resolve(db)
        });

    })
}

