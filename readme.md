# Projet-test-xp-back

## Contexte
L'objectif est de réaliser un agenda partagé qui permettra à une collectivité, ou une association de permettra à ses
membres/adhérents/communauté de rajouter des événements sur un calendrier qui pourra être consulter par n'importe qui.

## Contrainte de travail
L'idée va être de tenter d'appliquer les principes de l'extreme programming.
On va donc partir sur une gestion de planning et une conception Agile.

Définir toutes les user stories ("En tant que ... je veux pouvoir ... [afin de ...]) de l'application

Pour chaque user stories, définir ensembles les tests d'acceptances
Faire les maquettes fonctionnelles des user stories principales


Faire un planning poker afin de déterminer la "difficulté" de chaque user stories puis les ordonancer par importance (utiliser gitlab et ses issues pour mettre les résultats par écrit)
Pour la partie code, l'idée sera de faire ça en TDD et en Ping pong pair programming

## User stories

### En tant que personne ne faisant pas partie de l’association, je veux pouvoir consulter le planning pour prendre connaissance des événements prévus
-> consulter la page de l’agenda, voir les evenement si ils existent
Poker : 8
    
### En temps que nouveau membre, je souhaite m’inscrire pour pouvoir participer à l’élaboration des événements
    -> disposer du lien d’inscription (pas de bouton sur l’interface, envoi par mail)
        Poker : 13

### En temps qu membre de l’association, je souhaite me connecter afin d'accéder aux fonctionnalités
-> avoir un compte actif
    Poker : 5

### En tant que membre de l’association, je veux pouvoir ajouter un événement au planning pour le rendre visible à la communauté
-> Etre connecté 
-> Remplir les champs 
- début (date +heure)
- fin (date+heure)
- nom de l’évènement
- lieu de l’évènement
- (option) description de l'événement

->  Vérification de la création d’un nouvel objet en base
Poker : 5

### En temps que grosse collectivité, nous souhaitons pouvoir organiser plusieurs évènements simultanées pour profiter de nos effectifs importants
        -> Etre connecté
    -> Ajouter un événement avec des dates recoupant celles d’un événement déjà enregistré
Poker : 1


### En tant qu’adhérent.e je souhaiterai pouvoir modifier un événement pour tenir la communauté informée en cas d’erreur ou d'imprévu
    -> Etre connecté.e
    -> modifier un ou plusieurs champs et valider 
    ->  récupérer la liste des inscrits pour les informer du changement 
        Poker : 5

### En tant que membre de l’asso, je veux pouvoir annuler un événement en cas de problème
-> événement existant 
-> date non échue
-> événement pas encore annulé
        Poker : 5


--------------------------------------------------- FIN MVP----------------------------------------------------------------------------



### En tant qu’adhérent.e, je souhaite avoir accès à un historique pour avoir la liste des modifications apportées par qui à l’agenda
-> etre connecté
-> Consulter les logs
        -> logs : type de modif (ajout d'événement / modif d'événement +champ modifié / annulation d'événement), nom de l'événement, personne ayant modifié (+contenu de la modif?)

### En tant que membre de l’association je veux pouvoir m’inscrire à un événements pour faciliter l’organisation
-> Me connecter 
-> voir les évènements à venir

### En tant que membre de l’association je veux pouvoir me désinscrire d’un evénements pour faciliter l’organisation
-> Etre connecté.e
-> Afficher la liste des évènements auxquels je suis inscrit

### En tant qu’adhérent.e je souhaiterai pouvoir mettre des commentaires sur les différents évènements de l’agenda pour pouvoir echanger autour de l’évènement. (Visibilité restreinte aux adhérent.e.s)
-> Etre connecté.e
-> Avoir le droit d’écriture ? Si on est connecté c’est le cas je suppose...YES a mon avis -- Merci de cette intervention inconnu.e !
-> Soumis à une attente de validation ? modérateur ?

