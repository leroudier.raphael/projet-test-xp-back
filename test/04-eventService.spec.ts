import { expect } from "chai"
import { EventService } from "../src/service/eventService"
import { Event } from "../src/models/event";
import { ObjectId } from "mongodb";


describe('Test de eventService.checkInput()', () => {


    it('should return true when input is ok with everything', async () => {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": "details" }
        let toTest = EventService.checkInput(input)
        expect(toTest).to.be.true
    })

    it('should return true when input is ok without description', async () => {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": null }
        let toTest = EventService.checkInput(input)
        expect(toTest).to.be.true
    })

    it('should throw error when bad name in input', async () => {
        let input = { "name": 3, "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": "details" }
        expect(() => EventService.checkInput(input)).to.throw("bad name in input")
    })

    it('should throw error when bad start date in input', async () => {
        let input = { "name": "test", "startEvent": "test", "endEvent": new Date(), "location": "test", "details": "details" }
        expect(() => EventService.checkInput(input)).to.throw("bad start date in input")
    })

    it('should throw error when bad end date in input', async () => {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": "test", "location": "test", "details": "details" }
        expect(() => EventService.checkInput(input)).to.throw("bad end date in input")
    })

    it('should throw error when bad location in input', async () => {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": 3, "details": "details" }
        expect(() => EventService.checkInput(input)).to.throw("bad location in input")
    })

    it('should throw error when bad details in input', async () => {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": 3 }
        expect(() => EventService.checkInput(input)).to.throw("bad details in input")
    })
})

describe('Test if service retrieve incoming events from repo', () => {

    it('should retrive incoming events', async () => {

        let startDate = new Date();
        startDate.setDate(startDate.getDate() + 1);
        let endDate = new Date()
        endDate.setDate(startDate.getDate() + 1);

        let event = new Event("test", startDate, endDate, "Washington DC");

        EventService.save(event);

        let rep = await EventService.findIncoming();

        console.log('response',rep);
        expect(Array.isArray(rep)).to.be.equal(true);
        expect(rep.length).to.be.greaterThan(0)

    })


    it('should not return past events', async () => {

        let startEvent = new Date();
        startEvent.setDate(startEvent.getDate() - 1);
        let endEvent = new Date()
        endEvent.setDate(startEvent.getDate() - 1);

        let event = new Event("test", startEvent, endEvent, "Washington DC");

        EventService.save(event);

        let rep = await EventService.findIncoming();

        expect(rep.find(event => event.endEvent < new Date())).to.be.undefined;
    });

})

describe('Test save() in  EventService', () => {


    it('should create new full event', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");

        let rep = await EventService.save(event);
        expect(rep).to.have.property('_id');
        expect(rep._id instanceof ObjectId).to.be.true
        expect(rep).to.have.property('name');
        expect(rep.name).to.be.equal('test');
        expect(rep).to.have.property('startEvent');
        expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(rep).to.have.property('endEvent');
        expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(rep).to.have.property('location');
        expect(rep.location).to.be.equal('Washington DC');
        expect(rep).to.have.property('details');
        expect(rep.details).to.be.equal("Some details...");
        expect(rep instanceof Event).to.be.true

    });

    it('should create new event without details', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");

        let rep = await EventService.save(event);

        console.log('response',rep);

        expect(rep).to.have.property('_id');
        expect(rep._id instanceof ObjectId).to.be.true
        expect(rep).to.have.property('name');
        expect(rep.name).to.be.equal('test');
        expect(rep).to.have.property('startEvent');
        expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(rep).to.have.property('endEvent');
        expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(rep).to.have.property('location');
        expect(rep.location).to.be.equal('Washington DC');
        expect(rep instanceof Event).to.be.true
    });

})