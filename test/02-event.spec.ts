import { expect } from "chai";
import { Event } from "../src/models/event";
import { EventDao } from "../src/repository/eventDao";

describe('Test de event model', () => {
    it('should create new full event object', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");

        expect(event).to.have.property('_id');
        expect(event._id).to.not.equal(null);
        expect(event).to.have.property('name');
        expect(event.name).to.be.equal('test');
        expect(event).to.have.property('startEvent');
        expect(event.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(event).to.have.property('endEvent');
        expect(event.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(event).to.have.property('location');
        expect(event.location).to.be.equal('Washington DC');
        expect(event).to.have.property('details');
        expect(event.details).to.be.equal("Some details...");

    });    
    
    it('should create new event object without decription', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");

        expect(event).to.have.property('_id');
        expect(event._id).to.not.equal(null);
        expect(event).to.have.property('name');
        expect(event.name).to.be.equal('test');
        expect(event).to.have.property('startEvent');
        expect(event.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(event).to.have.property('endEvent');
        expect(event.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(event).to.have.property('location');
        expect(event.location).to.be.equal('Washington DC');
        expect(event).to.have.property('details');
        expect(event.details).to.be.equal(null);

    });
});
