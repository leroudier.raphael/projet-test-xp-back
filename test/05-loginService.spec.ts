import app from "../src/app";
import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const expect = chai.expect;

describe('Test du login', () => {

    it('new connection', (done) => {
        chai.request(app)
            .post('/login')
            .send({ mail: 'test@test.com', password: 'Azerty12356!' })
            .end((err, res) => {
                if (err) throw err;
                expect(res.body).to.be.property('token');
                globalThis.token = res.body.token
                globalThis.userConnected = res.body
                done()
            });
    });
});

