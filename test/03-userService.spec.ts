import { expect } from "chai";
import { User } from "../src/models/user";
import { UserService } from "../src/service/userService";


describe('Test de UserService', () => {

    it('find user ', async () => {
        const response = await UserService.findByMail("wiston@mail.fr");
        expect(response.mail).to.be.equal("wiston@mail.fr");
    });

    it('save new user with setPassword()', async () => {
        let user = new User("test", "test", "test", "test@test.com")
        user.password = 'Azerty12356!'
        user['password2'] = 'Azerty12356!'
        const response = await UserService.save(user);
        for (const key in response) {
            if (Object.prototype.hasOwnProperty.call(response, key)) {
                expect(response[key]).to.be.not.null
            }
        }
        expect(response.mail).to.be.equal("test@test.com");
        expect(response.password).to.be.not.equal(user.password);
        expect(response).to.be.not.equal(user.password);
        expect(response).to.not.be.property('password2');
    });


});
