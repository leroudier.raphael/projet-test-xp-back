import { expect } from "chai";
import { ObjectId } from "mongodb";
import { Event } from "../src/models/event";
import { EventDao } from "../src/repository/eventDao";


describe('Test of event DAO', () => {

    it('should create new full event', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");

        let rep = await EventDao.save(event);
        expect(rep).to.have.property('_id');
        expect(rep._id instanceof ObjectId).to.be.true
        expect(rep).to.have.property('name');
        expect(rep.name).to.be.equal('test');
        expect(rep).to.have.property('startEvent');
        expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(rep).to.have.property('endEvent');
        expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(rep).to.have.property('location');
        expect(rep.location).to.be.equal('Washington DC');
        expect(rep).to.have.property('details');
        expect(rep.details).to.be.equal("Some details...");

    });


    it('should create new event without details', async () => {

        let event = new Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");

        let rep = await EventDao.save(event);
        expect(rep).to.have.property('_id');
        expect(rep._id instanceof ObjectId).to.be.true
        expect(rep).to.have.property('name');
        expect(rep.name).to.be.equal('test');
        expect(rep).to.have.property('startEvent');
        expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        expect(rep).to.have.property('endEvent');
        expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        expect(rep).to.have.property('location');
        expect(rep.location).to.be.equal('Washington DC');

    });

    it('should return incomming events', async () => {

        let startDate = new Date();
        startDate.setDate(startDate.getDate() + 1);
        let endDate = new Date()
        endDate.setDate(startDate.getDate() + 1);

        let event = new Event("test", startDate, endDate, "Washington DC");

        EventDao.save(event);

        let rep = await EventDao.findIncoming();

        expect(Array.isArray(rep)).to.be.equal(true);
        expect(rep.length).to.be.greaterThan(0)
    });


    it('should not return past events', async () => {

        let startDate = new Date();
        startDate.setDate(startDate.getDate() - 1);
        let endDate = new Date()
        endDate.setDate(startDate.getDate() - 1);

        let event = new Event("test", startDate, endDate, "Washington DC");

        EventDao.save(event);

        let rep = await EventDao.findIncoming();

        expect(rep.find(event => event.endEvent < new Date())).to.be.undefined;
    });

});
