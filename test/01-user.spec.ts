import { expect } from "chai";
import { User } from "../src/models/user";
import { UserDao } from "../src/repository/userDao";

describe('Test de User model', () => {

    it('save user', async () => {
        const user = new User('Churchill', 'Wiston', 'Wiwi01', 'wiston@mail.fr')
        const rep = await UserDao.save(user)
        // expect(rep).to.be.instanceOf(User);
        expect(rep.name).to.be.equal('Churchill');
        expect(rep).to.have.property('_id');
        expect(rep).to.be.property('name');
        expect(rep).to.be.property('firstName');
        expect(rep).to.be.property('mail');
    });
    it('find user', async () => {
        const rep = await UserDao.find({ name: 'Churchill' })
        expect(rep.name).to.be.equal('Churchill');
        expect(rep).to.have.property('_id');
        expect(rep).to.be.property('name');
        expect(rep).to.be.property('firstName');
        expect(rep).to.be.property('mail');
    });


});
