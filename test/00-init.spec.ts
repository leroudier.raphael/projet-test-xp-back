import { getConnection } from "../configMongo";

before((done) => {
    
    setTimeout(async () => {
        const db = await getConnection()
        db.dropDatabase((err, result) => {
            if (err) console.log(err);
            done()
        });
    }, 5000);
});