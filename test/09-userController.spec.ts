import app from "../src/app";
import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const expect = chai.expect;

describe('Test user controller ', () => {

    it('save user / userService', (done) => {
        chai.request(app)
            .post('/user/save')
            .set('token', globalThis.token)
            .send({ mail: 'jean@jacques.com', password: 'Azerty12356!', password2: 'Azerty12356!' })
            .end((err, res) => {
                if (err) throw err;
                expect(res.status).to.be.equal(201)
                done()
            });
    })
    it('save user / userService', (done) => {
        chai.request(app)
            .post('/user/save')
            .send({ mail: 'jean2@jacques.com', password: 'Azerty12356!', password2: 'Azerty12356!' })
            .end((err, res) => {
                if (err) throw err;
                expect(res.status).to.be.equal(403)
                done()
            });
    })
})