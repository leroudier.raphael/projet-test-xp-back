"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const configMongo_1 = require("../configMongo");
before((done) => {
    setTimeout(() => __awaiter(void 0, void 0, void 0, function* () {
        const db = yield configMongo_1.getConnection();
        db.dropDatabase((err, result) => {
            if (err)
                console.log(err);
            done();
        });
    }), 5000);
});
//# sourceMappingURL=00-init.spec.js.map