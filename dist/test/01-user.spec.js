"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const user_1 = require("../src/models/user");
const userDao_1 = require("../src/repository/userDao");
describe('Test de User model', () => {
    it('save user', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = new user_1.User('Churchill', 'Wiston', 'Wiwi01', 'wiston@mail.fr');
        const rep = yield userDao_1.UserDao.save(user);
        // expect(rep).to.be.instanceOf(User);
        chai_1.expect(rep.name).to.be.equal('Churchill');
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep).to.be.property('name');
        chai_1.expect(rep).to.be.property('firstName');
        chai_1.expect(rep).to.be.property('mail');
    }));
    it('find user', () => __awaiter(void 0, void 0, void 0, function* () {
        const rep = yield userDao_1.UserDao.find({ name: 'Churchill' });
        chai_1.expect(rep.name).to.be.equal('Churchill');
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep).to.be.property('name');
        chai_1.expect(rep).to.be.property('firstName');
        chai_1.expect(rep).to.be.property('mail');
    }));
});
//# sourceMappingURL=01-user.spec.js.map