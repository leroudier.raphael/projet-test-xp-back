"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const eventService_1 = require("../src/service/eventService");
const event_1 = require("../src/models/event");
const mongodb_1 = require("mongodb");
describe('Test de eventService.checkInput()', () => {
    it('should return true when input is ok with everything', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": "details" };
        let toTest = eventService_1.EventService.checkInput(input);
        chai_1.expect(toTest).to.be.true;
    }));
    it('should return true when input is ok without description', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": null };
        let toTest = eventService_1.EventService.checkInput(input);
        chai_1.expect(toTest).to.be.true;
    }));
    it('should throw error when bad name in input', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": 3, "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": "details" };
        chai_1.expect(() => eventService_1.EventService.checkInput(input)).to.throw("bad name in input");
    }));
    it('should throw error when bad start date in input', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": "test", "endEvent": new Date(), "location": "test", "details": "details" };
        chai_1.expect(() => eventService_1.EventService.checkInput(input)).to.throw("bad start date in input");
    }));
    it('should throw error when bad end date in input', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": "test", "location": "test", "details": "details" };
        chai_1.expect(() => eventService_1.EventService.checkInput(input)).to.throw("bad end date in input");
    }));
    it('should throw error when bad location in input', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": 3, "details": "details" };
        chai_1.expect(() => eventService_1.EventService.checkInput(input)).to.throw("bad location in input");
    }));
    it('should throw error when bad details in input', () => __awaiter(void 0, void 0, void 0, function* () {
        let input = { "name": "test", "startEvent": new Date(), "endEvent": new Date(), "location": "test", "details": 3 };
        chai_1.expect(() => eventService_1.EventService.checkInput(input)).to.throw("bad details in input");
    }));
});
describe('Test if service retrieve incoming events from repo', () => {
    it('should retrive incoming events', () => __awaiter(void 0, void 0, void 0, function* () {
        let startDate = new Date();
        startDate.setDate(startDate.getDate() + 1);
        let endDate = new Date();
        endDate.setDate(startDate.getDate() + 1);
        let event = new event_1.Event("test", startDate, endDate, "Washington DC");
        eventService_1.EventService.save(event);
        let rep = yield eventService_1.EventService.findIncoming();
        console.log('response', rep);
        chai_1.expect(Array.isArray(rep)).to.be.equal(true);
        chai_1.expect(rep.length).to.be.greaterThan(0);
    }));
    it('should not return past events', () => __awaiter(void 0, void 0, void 0, function* () {
        let startEvent = new Date();
        startEvent.setDate(startEvent.getDate() - 1);
        let endEvent = new Date();
        endEvent.setDate(startEvent.getDate() - 1);
        let event = new event_1.Event("test", startEvent, endEvent, "Washington DC");
        eventService_1.EventService.save(event);
        let rep = yield eventService_1.EventService.findIncoming();
        chai_1.expect(rep.find(event => event.endEvent < new Date())).to.be.undefined;
    }));
});
describe('Test save() in  EventService', () => {
    it('should create new full event', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");
        let rep = yield eventService_1.EventService.save(event);
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep._id instanceof mongodb_1.ObjectId).to.be.true;
        chai_1.expect(rep).to.have.property('name');
        chai_1.expect(rep.name).to.be.equal('test');
        chai_1.expect(rep).to.have.property('startEvent');
        chai_1.expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(rep).to.have.property('endEvent');
        chai_1.expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(rep).to.have.property('location');
        chai_1.expect(rep.location).to.be.equal('Washington DC');
        chai_1.expect(rep).to.have.property('details');
        chai_1.expect(rep.details).to.be.equal("Some details...");
        chai_1.expect(rep instanceof event_1.Event).to.be.true;
    }));
    it('should create new event without details', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");
        let rep = yield eventService_1.EventService.save(event);
        console.log('response', rep);
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep._id instanceof mongodb_1.ObjectId).to.be.true;
        chai_1.expect(rep).to.have.property('name');
        chai_1.expect(rep.name).to.be.equal('test');
        chai_1.expect(rep).to.have.property('startEvent');
        chai_1.expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(rep).to.have.property('endEvent');
        chai_1.expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(rep).to.have.property('location');
        chai_1.expect(rep.location).to.be.equal('Washington DC');
        chai_1.expect(rep instanceof event_1.Event).to.be.true;
    }));
});
//# sourceMappingURL=04-eventService.spec.js.map