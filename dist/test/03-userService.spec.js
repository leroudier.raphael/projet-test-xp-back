"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const user_1 = require("../src/models/user");
const userService_1 = require("../src/service/userService");
describe('Test de UserService', () => {
    it('find user ', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield userService_1.UserService.findByMail("wiston@mail.fr");
        chai_1.expect(response.mail).to.be.equal("wiston@mail.fr");
    }));
    it('save new user with setPassword()', () => __awaiter(void 0, void 0, void 0, function* () {
        let user = new user_1.User("test", "test", "test", "test@test.com");
        user.password = 'Azerty12356!';
        user['password2'] = 'Azerty12356!';
        const response = yield userService_1.UserService.save(user);
        for (const key in response) {
            if (Object.prototype.hasOwnProperty.call(response, key)) {
                chai_1.expect(response[key]).to.be.not.null;
            }
        }
        chai_1.expect(response.mail).to.be.equal("test@test.com");
        chai_1.expect(response.password).to.be.not.equal(user.password);
        chai_1.expect(response).to.be.not.equal(user.password);
        chai_1.expect(response).to.not.be.property('password2');
    }));
});
//# sourceMappingURL=03-userService.spec.js.map