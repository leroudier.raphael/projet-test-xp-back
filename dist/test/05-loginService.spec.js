"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("../src/app"));
const chai_1 = __importDefault(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
chai_1.default.use(chai_http_1.default);
const expect = chai_1.default.expect;
describe('Test du login', () => {
    it('new connection', (done) => {
        chai_1.default.request(app_1.default)
            .post('/api/login')
            .send({ mail: 'test@test.com', password: 'Azerty12356!' })
            .end((err, res) => {
            if (err)
                throw err;
            expect(res.body).to.be.property('_id');
            expect(res.body).to.be.property('username');
            expect(res.body).to.be.property('token');
            globalThis.token = res.body.token;
            globalThis.userConnected = res.body;
            done();
        });
    });
});
//# sourceMappingURL=05-loginService.spec.js.map