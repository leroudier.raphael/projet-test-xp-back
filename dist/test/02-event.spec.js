"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const event_1 = require("../src/models/event");
describe('Test de event model', () => {
    it('should create new full event object', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");
        chai_1.expect(event).to.have.property('_id');
        chai_1.expect(event._id).to.not.equal(null);
        chai_1.expect(event).to.have.property('name');
        chai_1.expect(event.name).to.be.equal('test');
        chai_1.expect(event).to.have.property('startEvent');
        chai_1.expect(event.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(event).to.have.property('endEvent');
        chai_1.expect(event.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(event).to.have.property('location');
        chai_1.expect(event.location).to.be.equal('Washington DC');
        chai_1.expect(event).to.have.property('details');
        chai_1.expect(event.details).to.be.equal("Some details...");
    }));
    it('should create new event object without decription', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");
        chai_1.expect(event).to.have.property('_id');
        chai_1.expect(event._id).to.not.equal(null);
        chai_1.expect(event).to.have.property('name');
        chai_1.expect(event.name).to.be.equal('test');
        chai_1.expect(event).to.have.property('startEvent');
        chai_1.expect(event.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(event).to.have.property('endEvent');
        chai_1.expect(event.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(event).to.have.property('location');
        chai_1.expect(event.location).to.be.equal('Washington DC');
        chai_1.expect(event).to.have.property('details');
        chai_1.expect(event.details).to.be.equal(null);
    }));
});
//# sourceMappingURL=02-event.spec.js.map