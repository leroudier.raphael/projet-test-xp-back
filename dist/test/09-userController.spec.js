"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("../src/app"));
const chai_1 = __importDefault(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
chai_1.default.use(chai_http_1.default);
const expect = chai_1.default.expect;
describe('Test user controller ', () => {
    it('save user / userService', (done) => {
        chai_1.default.request(app_1.default)
            .post('/api/user/save')
            .set('token', globalThis.token)
            .send({ mail: 'jean@jacques.com', password: 'Azerty12356!', password2: 'Azerty12356!' })
            .end((err, res) => {
            if (err)
                throw err;
            expect(res.status).to.be.equal(201);
            done();
        });
    });
    it('save user / userService', (done) => {
        chai_1.default.request(app_1.default)
            .post('/api/user/save')
            .send({ mail: 'jean2@jacques.com', password: 'Azerty12356!', password2: 'Azerty12356!' })
            .end((err, res) => {
            if (err)
                throw err;
            expect(res.status).to.be.equal(403);
            done();
        });
    });
});
//# sourceMappingURL=09-userController.spec.js.map