"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const mongodb_1 = require("mongodb");
const event_1 = require("../src/models/event");
const eventDao_1 = require("../src/repository/eventDao");
describe('Test of event DAO', () => {
    it('should create new full event', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC", "Some details...");
        let rep = yield eventDao_1.EventDao.save(event);
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep._id instanceof mongodb_1.ObjectId).to.be.true;
        chai_1.expect(rep).to.have.property('name');
        chai_1.expect(rep.name).to.be.equal('test');
        chai_1.expect(rep).to.have.property('startEvent');
        chai_1.expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(rep).to.have.property('endEvent');
        chai_1.expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(rep).to.have.property('location');
        chai_1.expect(rep.location).to.be.equal('Washington DC');
        chai_1.expect(rep).to.have.property('details');
        chai_1.expect(rep.details).to.be.equal("Some details...");
    }));
    it('should create new event without details', () => __awaiter(void 0, void 0, void 0, function* () {
        let event = new event_1.Event("test", new Date('16 Dec 2020 11:00:00 GMT'), new Date('16 Dec 2020 12:00:00 GMT'), "Washington DC");
        let rep = yield eventDao_1.EventDao.save(event);
        chai_1.expect(rep).to.have.property('_id');
        chai_1.expect(rep._id instanceof mongodb_1.ObjectId).to.be.true;
        chai_1.expect(rep).to.have.property('name');
        chai_1.expect(rep.name).to.be.equal('test');
        chai_1.expect(rep).to.have.property('startEvent');
        chai_1.expect(rep.startEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 11:00:00 GMT');
        chai_1.expect(rep).to.have.property('endEvent');
        chai_1.expect(rep.endEvent.toUTCString()).to.be.equal('Wed, 16 Dec 2020 12:00:00 GMT');
        chai_1.expect(rep).to.have.property('location');
        chai_1.expect(rep.location).to.be.equal('Washington DC');
    }));
    it('should return incomming events', () => __awaiter(void 0, void 0, void 0, function* () {
        let startDate = new Date();
        startDate.setDate(startDate.getDate() + 1);
        let endDate = new Date();
        endDate.setDate(startDate.getDate() + 1);
        let event = new event_1.Event("test", startDate, endDate, "Washington DC");
        eventDao_1.EventDao.save(event);
        let rep = yield eventDao_1.EventDao.findIncoming();
        chai_1.expect(Array.isArray(rep)).to.be.equal(true);
        chai_1.expect(rep.length).to.be.greaterThan(0);
    }));
    it('should not return past events', () => __awaiter(void 0, void 0, void 0, function* () {
        let startDate = new Date();
        startDate.setDate(startDate.getDate() - 1);
        let endDate = new Date();
        endDate.setDate(startDate.getDate() - 1);
        let event = new event_1.Event("test", startDate, endDate, "Washington DC");
        eventDao_1.EventDao.save(event);
        let rep = yield eventDao_1.EventDao.findIncoming();
        chai_1.expect(rep.find(event => event.endEvent < new Date())).to.be.undefined;
    }));
});
//# sourceMappingURL=11-eventDao.spec.js.map