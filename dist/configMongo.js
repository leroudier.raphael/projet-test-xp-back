"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConnection = void 0;
const mongodb_1 = require("mongodb");
//const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'project_test';
//Options
var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 20
};
function getConnection() {
    return new Promise(function (resolve, reject) {
        return __awaiter(this, void 0, void 0, function* () {
            mongodb_1.MongoClient.connect(url, options, function (err, client) {
                const db = client.db(dbName);
                resolve(db);
            });
        });
    });
}
exports.getConnection = getConnection;
//# sourceMappingURL=configMongo.js.map