"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const mongodb_1 = require("mongodb");
const bcrypt_1 = __importDefault(require("bcrypt"));
const regexpPass = new RegExp(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/);
class User {
    constructor(name = null, firstName = null, pseudo = null, mail = null, data = null) {
        if (data && data._id && mongodb_1.ObjectId.isValid(data._id))
            this._id = typeof data._id == 'string' ? new mongodb_1.ObjectId(data._id) : data._id;
        else
            this._id = new mongodb_1.ObjectId();
        this._id = data && data._id ? data._id : new mongodb_1.ObjectId();
        this.name = data && data.name ? data.name : name;
        this.firstName = data && data.firstName ? data.firstName : firstName;
        this.pseudo = data && data.pseudo ? data.pseudo : pseudo;
        this.mail = data && data.mail ? data.mail : mail;
        if (data && data.password)
            this.password = data.password;
        if (data && data.password2)
            this.password2 = data.password2;
        if (data && data.roles)
            this.roles = data.roles;
    }
    light() {
        const tmp = {};
        for (const key in this) {
            if (this[key] && key != 'password')
                tmp['key'] = this[key];
        }
    }
    setPassword(password, salt = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!regexpPass.test(password))
                    throw new Error('Invalid password');
                this.password = salt == true ? yield getSalt(password) : password;
            }
            catch (err) {
                return err;
            }
        });
    }
}
exports.User = User;
function getSalt(password) {
    return new Promise(function (resolve, reject) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield bcrypt_1.default.hash(password, 10, function (err, hash) {
                    return __awaiter(this, void 0, void 0, function* () {
                        if (err)
                            throw err;
                        resolve(hash);
                    });
                });
            }
            catch (err) {
                reject(new Error(err));
            }
        });
    });
}
//# sourceMappingURL=user.js.map