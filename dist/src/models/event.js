"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Event = void 0;
const mongodb_1 = require("mongodb");
class Event {
    constructor(name, startEvent, endEvent, location, details = null, _id) {
        if (_id && mongodb_1.ObjectId.isValid(_id)) {
            this._id = typeof _id == 'string' ? new mongodb_1.ObjectId(_id) : _id;
        }
        else {
            this._id = new mongodb_1.ObjectId();
        }
        this.name = name;
        this.startEvent = startEvent;
        this.endEvent = endEvent;
        this.location = location;
        this.details = details;
    }
}
exports.Event = Event;
//# sourceMappingURL=event.js.map