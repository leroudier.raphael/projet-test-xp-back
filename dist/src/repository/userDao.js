"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDao = void 0;
const configMongo_1 = require("../../configMongo");
const user_1 = require("../models/user");
const collection = 'users';
class UserDao {
    static find(request) {
        return new Promise(function (resolve, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const db = yield configMongo_1.getConnection();
                    const response = yield db.collection(collection).findOne(request);
                    if (!response)
                        throw new Error("requête non trouvée");
                    resolve(new user_1.User(null, null, null, null, response));
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    }
    static save(user) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield configMongo_1.getConnection();
                const response = yield db.collection(collection).replaceOne({ _id: user._id }, user, { upsert: true });
                if (!response)
                    throw new Error("requête non trouvée");
                resolve(user);
            }
            catch (err) {
                reject(err);
            }
        }));
    }
}
exports.UserDao = UserDao;
//# sourceMappingURL=userDao.js.map