"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSchema = void 0;
exports.userSchema = {
    type: 'object',
    required: ['mail'],
    properties: {
        mail: { type: "string", minLength: 2, maxLength: 255, pattern: "[a-zA-ZàâæçéèêëîïöôœùûüÿÀÂÆÇnÉÈÊËÎÏÔÖŒÙÛÜŸ\s-]+" },
        name: { type: "string", minLength: 2, maxLength: 255, pattern: "[a-zA-ZàâæçéèêëîïöôœùûüÿÀÂÆÇnÉÈÊËÎÏÔÖŒÙÛÜŸ\s-]+" },
        firstName: { type: "string", minLength: 2, maxLength: 255, pattern: "[a-zA-ZàâæçéèêëîïöôœùûüÿÀÂÆÇnÉÈÊËÎÏÔÖŒÙÛÜŸ\s-]+" },
    }
};
//# sourceMappingURL=userSchema.js.map