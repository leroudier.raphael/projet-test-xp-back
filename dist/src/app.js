"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const router_1 = __importDefault(require("./routes/router"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = express_1.default();
const port = 3000;
app.use(helmet_1.default());
app.use(cors_1.default());
app.use(body_parser_1.default
    .urlencoded({ extended: true }));
app.use(body_parser_1.default
    .json({ limit: '50mb' }));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'content-type, Access-Control-Allow-Headers, Authorization, X-Requested-With, apitoken, usertoken, idfirm');
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    next();
});
app.use('/', router_1.default);
exports.default = app;
//# sourceMappingURL=app.js.map