"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventController = void 0;
const eventService_1 = require("../service/eventService");
class EventController {
    static save(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            eventService_1.EventService.save(req.body.event).then((x) => {
                return res.status(201).json(x);
            }).catch((error) => {
                return res.status(400).json({
                    error: error
                });
            });
        });
    }
    static findIncoming(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            eventService_1.EventService.findIncoming().then((x) => {
                return res.status(201).json(x);
            }).catch((error) => {
                return res.status(400).json({
                    error: error
                });
            });
        });
    }
}
exports.EventController = EventController;
//# sourceMappingURL=eventController.js.map