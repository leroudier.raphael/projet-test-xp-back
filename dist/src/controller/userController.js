"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const userService_1 = require("../service/userService");
class UserController {
    static save(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            userService_1.UserService.save(req.body).then((x) => {
                return res.status(201).json(x);
            }).catch((error) => {
                return res.status(400).json({
                    error: error
                });
            });
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=userController.js.map