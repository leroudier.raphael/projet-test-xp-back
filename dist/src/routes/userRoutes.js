"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const userController_1 = require("../controller/userController");
const usersRouter = express_1.Router();
usersRouter.post('/save', userController_1.UserController.save);
// usersRouter.post('/findid', UserController.findId)
exports.default = usersRouter;
//# sourceMappingURL=userRoutes.js.map