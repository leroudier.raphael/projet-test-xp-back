"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const loginController_1 = require("../controller/loginController");
const userMW_1 = require("../middleWare/userMW");
const eventRoutes_1 = __importDefault(require("./eventRoutes"));
const userRoutes_1 = __importDefault(require("./userRoutes"));
const router = express_1.Router();
router.use('/login', loginController_1.login);
router.use('/user', userMW_1.authenticated, userRoutes_1.default);
router.use('/event', eventRoutes_1.default);
exports.default = router;
//# sourceMappingURL=router.js.map