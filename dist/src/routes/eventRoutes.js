"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const eventController_1 = require("../controller/eventController");
const userMW_1 = require("../middleWare/userMW");
const eventsRouter = express_1.Router();
eventsRouter.get('/findall', eventController_1.EventController.findIncoming);
eventsRouter.post('/save', userMW_1.authenticated, eventController_1.EventController.save);
exports.default = eventsRouter;
//# sourceMappingURL=eventRoutes.js.map