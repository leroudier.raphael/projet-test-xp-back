"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const user_1 = require("../models/user");
const userDao_1 = require("../repository/userDao");
const userSchema_1 = require("../schema/userSchema");
const ajv_1 = __importDefault(require("ajv"));
const ajv = new ajv_1.default({ allErrors: true });
class UserService {
    static save(input) {
        return new Promise(function (resolve, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    if (!ajv.validate(userSchema_1.userSchema, input))
                        throw new Error(JSON.stringify(ajv.errors));
                    const user = new user_1.User(null, null, null, null, input);
                    if (input.password && input['password2'] && input.password === input['password2']) {
                        yield user.setPassword(input.password, true);
                        delete user.password2;
                    }
                    for (const key in user) {
                        if (user[key] == null)
                            delete user[key];
                    }
                    // not null proerty
                    yield userDao_1.UserDao.save(user);
                    resolve(user);
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    }
    static findByMail(mail) {
        return new Promise(function (resolve, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    let response = yield userDao_1.UserDao.find({ 'mail': mail });
                    resolve(response);
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    }
}
exports.UserService = UserService;
//# sourceMappingURL=userService.js.map