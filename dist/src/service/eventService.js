"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventService = void 0;
const event_1 = require("../models/event");
const eventDao_1 = require("../repository/eventDao");
class EventService {
    static save(input) {
        if (this.checkInput(input)) {
            let event = new event_1.Event(input["name"], input["startEvent"], input["endEvent"], input["location"], input["details"]);
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let response = yield eventDao_1.EventDao.save(event);
                        resolve(response);
                    }
                    catch (err) {
                        reject(err);
                    }
                });
            });
        }
    }
    static findIncoming() {
        return new Promise(function (resolve, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    let response = yield eventDao_1.EventDao.findIncoming();
                    console.log("here 1 response ->", response);
                    let eventArray = [];
                    for (let i = 0; i < response.length; i++) {
                        eventArray[i] = yield response[i].map((x) => { return new event_1.Event(x.name, x.startEvent, x.endEvent, x.location, x.details, x._id); });
                    }
                    console.log("here 2 event array->", eventArray);
                    resolve(eventArray);
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    }
    static checkInput(input) {
        if (typeof input["name"] != 'string') {
            throw new Error("bad name in input");
        }
        if (Object.prototype.toString.call(input["startEvent"]) != '[object Date]') {
            throw new Error("bad start date in input");
        }
        if (Object.prototype.toString.call(input["endEvent"]) != '[object Date]') {
            throw new Error("bad end date in input");
        }
        if (typeof input["location"] != 'string') {
            throw new Error("bad location in input");
        }
        if (typeof input["details"] != 'string' && input["details"] != null) {
            throw new Error("bad details in input");
        }
        return true;
    }
}
exports.EventService = EventService;
//# sourceMappingURL=eventService.js.map