"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginService = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const userDao_1 = require("../repository/userDao");
const jwtService_1 = require("../service/jwtService");
const mongodb_1 = require("mongodb");
const EMAIL_REGEX = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const PASS_REGEX = new RegExp(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/);
class LoginService {
    static login(mail = null, password = null, token = null) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                let user;
                if (!token) {
                    if (!EMAIL_REGEX.test(mail))
                        throw new Error('Mail Invalid');
                    if (!PASS_REGEX.test(password))
                        throw new Error('Password Invalid');
                    user = yield userDao_1.UserDao.find({ 'mail': mail });
                    if (!user)
                        throw new Error('user Invalid');
                    let bcryptRep = yield bcrypt_1.default.compare(password, user.password);
                    if (!bcryptRep)
                        throw new Error('Password ou mail erroné');
                }
                else {
                    if (!token)
                        throw new Error('Token Invalid');
                    let resultToken = yield jwtService_1.jwtService.verifToken(token);
                    if (!resultToken)
                        throw new Error('jwtService error');
                    user = yield userDao_1.UserDao.find({ _id: new mongodb_1.ObjectId(resultToken['_id']) });
                }
                resolve({
                    user: user.light(),
                    token: jwtService_1.jwtService.generateToken(user),
                });
            }
            catch (err) {
                reject(err);
            }
        }));
    }
}
exports.LoginService = LoginService;
//# sourceMappingURL=loginService.js.map