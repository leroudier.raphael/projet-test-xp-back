"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jwtService = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const JWT_SIGN_SECRET = 'RT3zxQ2Fn3A3j666fNj6r4u7Ec8gVLYsCW9UD78HZE5D63pJU69gen35grnPyj8E433sUY525Je3hZwTSgumJ7m3MZzi233NgGfd';
class jwtService {
    static verifToken(token) {
        const that = this;
        return new Promise(function (resolve, reject) {
            try {
                token = that.parseToken(token);
                let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (!jwtToken)
                    throw new Error("Token User Invalid");
                resolve(jwtToken);
            }
            catch (err) {
                reject(err);
            }
        });
    }
    static generateToken(user) {
        return jwt.sign({
            _id: user._id,
        }, JWT_SIGN_SECRET, {
            expiresIn: "10d",
            algorithm: "HS512",
        });
    }
    static parseToken(token) {
        return token.replace('Bearer ', '');
    }
}
exports.jwtService = jwtService;
//# sourceMappingURL=jwtService.js.map