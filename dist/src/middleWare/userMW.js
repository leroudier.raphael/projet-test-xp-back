"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticated = void 0;
const mongodb_1 = require("mongodb");
const userDao_1 = require("../repository/userDao");
const jwtService_1 = require("../service/jwtService");
function authenticated(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const token = req.headers.token;
            if (!token)
                throw new Error('No token User');
            let reponse = yield jwtService_1.jwtService.verifToken(token);
            if (!reponse)
                throw new Error('Token Invalid');
            let user = yield userDao_1.UserDao.find({ "_id": new mongodb_1.ObjectId(reponse['_id']) });
            if (!user)
                throw new Error('User Invalid');
            next();
        }
        catch (err) {
            return res.status(403).json(err);
        }
    });
}
exports.authenticated = authenticated;
//# sourceMappingURL=userMW.js.map